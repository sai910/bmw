import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


//引入element
import ElementUI from 'element-ui';
//引入css
import 'element-ui/lib/theme-chalk/index.css';


//引入axios
import axios from 'axios'
import http from './assets/js/http.js'

//引入lazyload
import VueLazyLoad from 'vue-lazyload'


//引入bus
import bus from './utils/index';

Vue.use(ElementUI);

//引入完插件之后

//在vue中使用这个插件
Vue.use(VueLazyLoad,{
  loading:'/mi-shop-pic/loading-svg/loading-bars.svg'
})

//loading图 设置懒加载时候的loading图

// svg 类似图片
// 区别:svg矢量图 是不会因为拉伸导致图片失真

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$http = http
Vue.prototype.$bus = bus

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


//直接安装，在packjson是没有显示你安装过的插件，
//换句话说，别人不知道你在这个项目中用了什么插件
//所以，通常安装插件的时候，    npm install axios --save-dev
//--save-dev 就会将插件名称保存在packjson，方便别人看你的代码