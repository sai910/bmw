import api from '../js/api'
import http from '../js/http'

// 将请求封装好了之后，暴露出去，通过传递参数的方式去请求
// 封装get请求

export function getProducts() {
    return http.get(
        api.products, {
        params: {
            categoryId: '100012'
        }
    }
    )
}

export function get(url, params) {
    return new Promise((resolve, reject) => {
        http.get(url, {
            params
        }).then((res) => {
            // console.log(res);
            resolve(res)
        }).catch((res) => {
            // console.log(err);
            reject(err)
        })
    })
}

//NavHeader请求
export function getNavHeader(projectID, classifyID) {
    return http
        .get(api.products, {
            params: {
                project_id: projectID,
                classify_id: classifyID,
            }
        })
}

//NavHeader页面购物车数量请求
export function cartQuantity() {
    return http.get(api.cart,{
        params:{
          project_Id:47
        }
      })
}


//index页面图片请求
export function getIndex(idOne, idTwo) {
    return http.get(api.products, {
        params: {
            project_id: idOne,
            classify_id: idTwo
        }
    })
}

//index页面添加商品到购物车
export function addGoodsCart(id) {
    return http.post(api.cart, {
            goods_id: id,
            num: 1
        })
}