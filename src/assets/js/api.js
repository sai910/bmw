
console.log(process.env.NODE_ENV);

// 当前你的项目处于development  开发环境/api通过代理 http://shopback.bluej.cn/api  //测试地址
//              production   生产环境 http://shopback.bluej.cn/api   //真实地址
// 现在我们知道如何判断环境，根据环境设置不同的请求地址    
const baseUrl = (process.env.NODE_ENV == 'development')?'/api':'http://shopback.bluej.cn/api'  

// const baseUrl = '/api';
console.log(baseUrl);

//proxy在生产环境中是没有效果的
module.exports = {
    products:baseUrl + '/goods',//产品接口
    cart:baseUrl + '/shoppingCart',//购物车接口
    carousel:baseUrl + '/carousel',//轮播图接口
    register:'/user/register',//注册接口
    login:baseUrl + '/login',//登录接口
    city:baseUrl + '/city',//城市获取
    order:baseUrl + '/order',//新增订单
    delete:baseUrl + '/order',//删除订单
    address:baseUrl + '/address'//收货人地址
}