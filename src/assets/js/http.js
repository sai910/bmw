// 安装 axios
//引入
import axios from 'axios'

// 建立一个axios实例
const http = axios.create();

//请求拦截
http.interceptors.request.use(function(config){
    let token = localStorage.getItem('token') || null;
    if(token){
        config.headers['x-token'] = token
    }
    return config
})

// 返回数据的模式
// {
//     status:0,非0
//     data:[], msg
// }

//配置响应拦截
http.interceptors.response.use(function(response){
    const res = response.data
    //这里的data是服务器的data
    //在这里通过res的设置统一报错，也是方便我们开发
    //提升开发效率
    return res.result
})

export default http;