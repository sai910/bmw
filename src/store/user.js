export default {
    namespaced:true,
    state: {
        isLogin: localStorage.getItem('isLogin') || false,
        loginName: localStorage.getItem('uname'),
        xtoken: localStorage.getItem('token'),
    },
    getters: {
        loginName:state => state.loginName + '欢迎你'
    },
    mutations: {
        changeIsLogin(state, v) {
            localStorage.setItem('isLogin', v)
            state.isLogin = v
        },
        changeLoginName(state, n) {
            localStorage.setItem('uname', n)
            state.loginName = n
        },
        getXtoken(state, token) {
            localStorage.setItem('token', token)
            state.xtoken = token
        }
    },
    actions: {
    },
}