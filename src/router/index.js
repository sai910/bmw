import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import login from '../views/login.vue'
import cart from '../views/cart.vue'
import register from '../views/register.vue'


//二级路由
import index from '../views/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path:'/end',
    name:'end',
    component:() => import('../views/end.vue'),
  },
  {
    path:'/',
    name:'Home',
    component:Home,
    redirect:'/index',
    children:[
      {
        path:'index',
        name:'index',
        component:index
      },
      {
        path:'product/:id',
        name:'product',
        component:() => import('../views/product.vue'),
        beforeEnter: (to, from, next) => {
      
          if (localStorage.getItem('isLogin')) {
                  
                  next()
                } else {
                  
                  next('/Login?redirect=' + to.fullPath)
                  
                }
        }
      },
      {
        path:'detail/:id',
        name:'detail',
        component:() => import('../views/detail.vue')
      }
    ]
  },
  {
    path:'/order',
    name:'order',
    component:() => import ('../views/order.vue'),
    children:[
      {
        path:'orderConfirm',
        name:'orderConfirm',
        component:() => import ('../views/orderConfirm.vue')
      },
      {
        path:'orderList',
        name:'orderList',
        component:() => import ('../views/orderList.vue')
      },
      {
        path:'orderPay',
        name:'orderPay',
        component:() => import ('../views/orderPay.vue')
      }
    ]
  },
  {
    path:'/login',
    name:'login',
    component:login
  },
  {
    path:'/cart',
    name:'cart',
    component:cart,
    beforeEnter: (to, from, next) => {
      
      if (localStorage.getItem('isLogin')) {
              
              next()
            } else {
              
              next('/Login?redirect=' + to.fullPath)
              
            }
    }
  },
  {
    path:'/register',
    name:'register',
    component:register
  },
]

const router = new VueRouter({
  routes
})

export default router
