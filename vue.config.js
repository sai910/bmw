module.exports = {
  lintOnSave: false,
  devServer:{
    proxy:{//设置本地服务器配置
      '/api':{
        host:'localhost',
        port:8080,
        // target:'http://mall-pre.springboot.cn',
        target:'http://shopback.bluej.cn/api',
        changeOrigin:true,  //允许跨域
        pathRewrite:{
          '^/api':""
        }
      }
    }
  }
}



//如何使用 当我匹配到/api 把localhost:8080 => target

//内置的后台 => 真实后台 注意：原理是后台之间通讯是没有跨域

// localhost:8080/api/carts

// 下面的请求不是从浏览器发送的，而是在内置服务器上发送
// http://mall-pre.springboot.cn/carts

// 设置完之后一定要重启项目